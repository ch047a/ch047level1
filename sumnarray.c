//Write a function to find sum of n different numbers (Use arrays)
#include<stdio.h>
int input()
{
    int n;
    printf("Enter how many number you want to add:\n");
    scanf("%d",&n);
    return n;
}
int arr(int n)
{
    int arr[n],sum=0;
    for(int c=1;c<=n;c++)
    {
        printf("    Enter the %dth number:",c);
        scanf("%d",&arr[c]);
        sum+=arr[c];
    }
    printf("The Sum of Arrays is:\t %d",sum);
}
void main()
{
    int p;
    p=input();
    arr(p);
}
/*Credits: Shreesh and Chaitanya */